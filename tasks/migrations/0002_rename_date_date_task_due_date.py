# Generated by Django 4.1.2 on 2022-10-25 03:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="task",
            old_name="date_date",
            new_name="due_date",
        ),
    ]
